Hello! Welcome to my Backend Test Project.

To run this project on your local environment, you need:

1. clone this project: `git clone https://gitlab.com/nelaneliyuna/miniproject.git`
2. open the directory of the cloned project, `cd miniproject`
3. install this project: `npm install`
4. edit config/config.json. The username should be your local username. Change the database names to reflect the actual project.
5. Create the development database using `createdb [your db name]` on the command line.
7. run `sequelize db:migrate`
8. for running the project: `npm start`
9. open postman for executing the project
10. Part A: run `GET localhost:3000/covid` to test the api
11. Part B: Run mini project: `npm start`; Run scheduler: `nodemon scheduler.js`

