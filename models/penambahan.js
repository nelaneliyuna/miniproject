'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Penambahan extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Penambahan.init({
    jumlah_positif: DataTypes.INTEGER,
    jumlah_meninggal: DataTypes.INTEGER,
    jumlah_sembuh: DataTypes.INTEGER,
    jumlah_dirawat: DataTypes.INTEGER,
    tanggal: DataTypes.DATE,
    created: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Penambahan',
  });
  return Penambahan;
};