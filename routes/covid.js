var express = require('express');
var router = express.Router();
var axios = require('axios');
const models = require('../models')
const Penambahan = models.Penambahan
const cron = require('node-cron');

router.get('/', async(req, res, next) =>{
    let data_covid = await axios('https://data.covid19.go.id/public/api/update.json');
    if(data_covid.data.update.penambahan){
        await Penambahan.create(data_covid.data.update.penambahan)
        let data_penambahan = await Penambahan.findAll({raw: true})
        res.send({
            data_penambahan
        })
    }
    else{
        res.status(500)
    }
});

// router.get('/scheduler', async(req, res, next) =>{
//     cron.schedule('59 23 * * *', async () => {
//         let data_covid = await axios('https://data.covid19.go.id/public/api/update.json');
//         if(data_covid.data.update.penambahan){
//             let createData = await Penambahan.create(data_covid.data.update.penambahan)
//             console.log("running scheduler for fetching and saving data of additional covid: ", createData)
//         }
//         else{
//             res.status(500)
//         }
//     });   
// });

module.exports = router;
